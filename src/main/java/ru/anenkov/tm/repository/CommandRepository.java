package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.ICommandRepository;
import ru.anenkov.tm.constant.ArgumentConst;
import ru.anenkov.tm.constant.LiteralConst;
import ru.anenkov.tm.constant.TerminalConst;
import ru.anenkov.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            TerminalConst.HELP, ArgumentConst.HELP, LiteralConst.HELP
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, LiteralConst.ABOUT
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            TerminalConst.VERSION, ArgumentConst.VERSION, LiteralConst.VERSION
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            TerminalConst.INFO, ArgumentConst.INFO, LiteralConst.INFO
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            TerminalConst.EXIT, null, LiteralConst.EXIT
    );

    public static final TerminalCommand ARGUMENT = new TerminalCommand(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, LiteralConst.ARGUMENTS
    );

    public static final TerminalCommand COMMAND = new TerminalCommand(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, LiteralConst.COMMANDS
    );

    private static final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, VERSION, ABOUT, INFO, ARGUMENT, COMMAND, EXIT
    };

    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand current : values) {
            final String name = current.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (TerminalCommand current : values) {
            final String arg = current.getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}

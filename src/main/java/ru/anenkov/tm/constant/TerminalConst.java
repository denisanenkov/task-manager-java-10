package ru.anenkov.tm.constant;

public interface TerminalConst {

    String HELP = "Help";

    String VERSION = "Version";

    String ABOUT = "About";

    String EXIT = "Exit";

    String INFO = "Info";

    String ARGUMENTS = "Arguments";

    String COMMANDS = "Commands";

}

package ru.anenkov.tm.constant;

public interface LiteralConst {

    String HELP = "Display terminal commands.";

    String VERSION = "Show version info.";

    String ABOUT = "Show developer info.";

    String EXIT = "Close application.";

    String INFO = "Display information about system.";

    String ARGUMENTS = "Show program arguments.";

    String COMMANDS = "Show program commands.";

}

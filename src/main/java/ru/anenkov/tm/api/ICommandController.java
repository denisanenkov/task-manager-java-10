package ru.anenkov.tm.api;

public interface ICommandController {

    void showVersion();

    void showInfo();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void exit();

}

package ru.anenkov.tm.api;

import ru.anenkov.tm.model.TerminalCommand;

public interface ICommandService {

    String[] getCommands();

    String[] getArgs();

    TerminalCommand[] getTerminalCommands();

}

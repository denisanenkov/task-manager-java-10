package ru.anenkov.tm.service;

import ru.anenkov.tm.api.ICommandRepository;
import ru.anenkov.tm.api.ICommandService;
import ru.anenkov.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}

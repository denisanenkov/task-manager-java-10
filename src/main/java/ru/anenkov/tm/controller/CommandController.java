package ru.anenkov.tm.controller;

import ru.anenkov.tm.api.ICommandController;
import ru.anenkov.tm.api.ICommandRepository;
import ru.anenkov.tm.api.ICommandService;
import ru.anenkov.tm.model.TerminalCommand;
import ru.anenkov.tm.repository.CommandRepository;
import ru.anenkov.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    ICommandRepository commandRepository = new CommandRepository();

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.8");
    }

    public void showInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Denis Anenkov");
        System.out.println("E-MAIL: denk.an@inbox.ru");
    }

    public void showCommands() {
        final String[] commands = commandRepository.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void showArguments() {
        final String[] arguments = commandRepository.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    public void showHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = commandRepository.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    public void exit() {
        System.exit(0);
    }

}
